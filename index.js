const express = require("express");
const bodyparser = require("body-parser");
const jsonparser = bodyparser.json();
const app = express();
const port = "3000";
const path = require("path");

app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');
app.use(express.static(__dirname + '/public'));

app.get("/", (req, res) => {
    res.sendFile(path.join(__dirname, './view/chapter3/index.html'));
})

app.get("/suit", (req, res) => {
    res.sendFile(path.join(__dirname, './view/chapter4/index.html'));
})

let user = [];

const verifyAuthToken = (req, res, next) => {
    let auth = req.headers['authorization'];
    let obj = user.find(o => o.token === auth)
    if (obj) {
        res.user = obj;
        next();
    } else {
        res.status(401).json({
            "message": "Unauthorized"
        });
    }
}

app.post('/register', jsonparser, (req, res) => {
    let body = req.body;
    let email = body.email;
    let password = body.password;
    let r = (Math.random() + 1).toString(36).substring(7);

    user.push({
        email,
        password,
        token: r
    });
    console.log(user);

    res.send({
        message: "Register berhasil"
    });
});

app.post('/login', jsonparser, (req, res) => {
    let body = req.body;
    let email = body.email;
    let password = body.password;

    let obj = user.find(o => o.email === email)

    if (obj && obj.password === password) {
        res.send({
            message: "Login Berhasil",
            token: obj.token
        });
    } else {
        res.send({
            message: "Login Berhasil"
        });
    }
});


app.post('/profile', jsonparser, verifyAuthToken, (req, res) => {
    res.send({
        email: res.user.email
    });
});






app.listen(port, () => {
    console.log(`App listening at http://localhost:${port}`);
});