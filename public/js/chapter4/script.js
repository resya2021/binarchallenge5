class suit {

    WinCondition = "";
    constructor(choice, comchoice, ) {
        this.choice = choice;
        this.comchoice = comchoice;
    }
    getuserchoice() {
        return this.choice
    }
    getcomputerchoice() {
        return this.comchoice
    }

}
let choiceset = ["gunting", "batu", "kertas"];
const rock_user = document.getElementById('r_user')
const scissor_user = document.getElementById('s_user')
const paper_user = document.getElementById('p_user')
const rock_comp = document.getElementById('r-com')
const paper_comp = document.getElementById('p-com')
const scissor_comp = document.getElementById('s-com')
const winBox = document.getElementById('box');
const inFo = document.getElementById("H1");
const refresh = document.getElementById("refreshbut")
const addElement1 = [...document.getElementsByClassName("dissap")];
const button = document.querySelector('button');

let baru = new suit();

function comThink() {
    var choices = ['Batu', 'Gunting', 'Kertas'];
    var randomChoices = Math.floor(Math.random() * 3);
    return choices[randomChoices];

}

//Color Change 
function resultObject() {
    winBox.classList.add('winBox'),
        inFo.setAttribute("style", "font-size:36px; color:white;");

}

function resultDraw() {
    winBox.classList.add('drawBox');

    inFo.setAttribute("style", "font-size:36px; color:white;");

}

function lose() {
    console.log("COM WIN");
    resultObject();

    inFo.innerText = "COM WIN"
}

//Text Win or Lose or Draw BOX
function win() {
    console.log("Player 1 Win");
    resultObject();
    inFo.innerText = "Player 1 WIN"

}

function draw() {
    console.log("Draw");
    resultDraw();

    inFo.innerText = "Draw"
}


// gameCompare
function gameCompare(pilihanUser) {

    const computerUser = this.comThink();
    console.log("Hasil User => " + pilihanUser);
    console.log("Hasil dari => " + computerUser);

    switch (pilihanUser + computerUser) {
        case "BatuGunting":
        case "GuntingKertas":
        case "KertasBatu":
            win();

            break;
        case "GuntingBatu":
        case "BatuKertas":
        case "KertasGunting":
            lose();

            break;
        case "GuntingGunting":
        case "BatuBatu":
        case "KertasKertas":
            draw();

    }


    switch (computerUser) {
        case "Batu":
            rock_comp.classList.add('chosen');

            break;
        case "Gunting":
            scissor_comp.classList.add('chosen');
            break;
        case "Kertas":
            paper_comp.classList.add('chosen');


    }

}

//Human Choice
function play() {
    rock_user.addEventListener('click', function () {
        this.classList.add('chosen');
        gameCompare("Batu");
        addElement1.forEach(addElement3 => {
            addElement3.setAttribute("style", "cursor: not-allowed;pointer-events: none;")
        })


    })
    paper_user.addEventListener('click', function () {
        this.classList.add('chosen');
        gameCompare("Kertas");
        addElement1.forEach(addElement3 => {
            addElement3.setAttribute("style", "cursor: not-allowed;pointer-events: none;")
        })

    })
    scissor_user.addEventListener('click', function () {
        this.classList.add('chosen');
        gameCompare("Gunting");
        addElement1.forEach(addElement3 => {
            addElement3.setAttribute("style", "cursor: not-allowed;pointer-events: none;")
        })

    })
}
play()

// Refresh Listemner
refresh.addEventListener('click', function () {
    //window.location.reload();

    addElement1.forEach(addElement2 => {
        addElement2.classList.remove('chosen')
    });
    addElement1.forEach(addElement3 => {
        addElement3.removeAttribute("style", "cursor: not-allowed;pointer-events: none;")

    })
    winBox.classList.remove('winBox');
    winBox.classList.remove('drawBox');
    inFo.removeAttribute("style", "color: ''; font-size:'' ")

    inFo.style.marginTop = null
    inFo.style.fontSize = null
    inFo.innerText = "VS"
    button.disabled = false;
})